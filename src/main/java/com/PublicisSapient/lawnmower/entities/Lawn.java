package com.PublicisSapient.lawnmower.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Lawn {
    private Position start;
    private List<Character> instructions;
}