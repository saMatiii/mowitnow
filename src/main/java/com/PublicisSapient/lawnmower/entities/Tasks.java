package com.PublicisSapient.lawnmower.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Tasks {
    private Coordinates dimentions;
    private List<Lawn> tasks;
}
