package com.PublicisSapient.lawnmower.entities;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Coordinates {
    private Integer x;
    private Integer y;
}
