package com.PublicisSapient.lawnmower.entities;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Position {
    private Coordinates coordinates;
    private Direction direction;
}
