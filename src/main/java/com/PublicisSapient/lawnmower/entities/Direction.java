package com.PublicisSapient.lawnmower.entities;

import com.PublicisSapient.lawnmower.core.*;

import java.util.function.Supplier;

public enum Direction {
    N(North::new),
    S(South::new),
    W(West::new),
    E(East::new);

    private final Supplier<DirectionCheck> direction;

    Direction(Supplier<DirectionCheck> direction) {
        this.direction = direction;
    }

    public Supplier<DirectionCheck> getDirection() {
        return this.direction;
    }
}
