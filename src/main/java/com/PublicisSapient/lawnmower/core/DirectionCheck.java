package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;

public interface DirectionCheck {
    /**
     * This methode checks if the Lawn next move is not outpassing limits
     *
     * @param coordinates
     * @param dimensions
     * @return
     */
    boolean isNextPositionOut(Coordinates coordinates, Coordinates dimensions);


    /**
     * This methodes changes the direction of the Lawn
     *
     * @param toString
     * @return the neww direction
     */
    Direction redirect(String toString);


    /**
     * this methode is moving the Lawn from one point to another
     *
     * @param oldCoordinates
     * @return
     */
    Coordinates makeaMove(Coordinates oldCoordinates);
}
