package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;

public class West implements DirectionCheck {

    @Override
    public boolean isNextPositionOut(Coordinates coordinates, Coordinates dimension) {
        return coordinates.getX() - 1 < 0 ? true : false;
    }

    @Override
    public Direction redirect(String newDirection) {
        switch (newDirection) {
            case "G":
                return Direction.S;
            case "D":
                return Direction.N;
            default:
                return null;
        }
    }

    @Override
    public Coordinates makeaMove(Coordinates oldCoordinates) {
        return Coordinates.builder().x(oldCoordinates.getX() - 1).y(oldCoordinates.getY()).build();
    }
}