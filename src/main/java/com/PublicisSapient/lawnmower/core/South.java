package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;

public class South implements DirectionCheck {

    @Override
    public boolean isNextPositionOut(Coordinates coordinates, Coordinates dimension) {
        return coordinates.getY() - 1 < 0 ? true : false;
    }

    @Override
    public Direction redirect(String newDirection) {
        switch (newDirection) {
            case "G":
                return Direction.E;
            case "D":
                return Direction.W;
            default:
                return null;
        }
    }

    @Override
    public Coordinates makeaMove(Coordinates oldCoordinates) {
        return Coordinates.builder().x(oldCoordinates.getX()).y(oldCoordinates.getY() - 1).build();
    }
}
