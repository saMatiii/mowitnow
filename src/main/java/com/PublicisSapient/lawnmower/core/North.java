package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;

public class North implements DirectionCheck {

    @Override
    public boolean isNextPositionOut(Coordinates coordinates, Coordinates dimension) {
        return coordinates.getY() + 1 > dimension.getY() ? true : false;
    }

    @Override
    public Direction redirect(String newDirection) {
        switch (newDirection) {
            case "G":
                return Direction.W;
            case "D":
                return Direction.E;
            default:
                return null;
        }
    }

    @Override
    public Coordinates makeaMove(Coordinates oldCoordinates) {
        return Coordinates.builder().x(oldCoordinates.getX()).y(oldCoordinates.getY() + 1).build();
    }
}
