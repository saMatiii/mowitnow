package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Direction;

public class DirectionFactory {

    /**
     * This methode return a direction object of the actual position which we will use for
     * checks related to redirecting or moving forword
     *
     * @param direction
     * @return
     */
    public static DirectionCheck getDirectionObject(Direction direction) {
        return direction.getDirection().get();
    }

}
