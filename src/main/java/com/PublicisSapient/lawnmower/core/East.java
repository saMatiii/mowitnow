package com.PublicisSapient.lawnmower.core;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;

public class East implements DirectionCheck {

    @Override
    public boolean isNextPositionOut(Coordinates coordinates, Coordinates dimension) {
        return coordinates.getX() + 1 > dimension.getX() ? true : false;
    }

    @Override
    public Direction redirect(String newDirection) {
        switch (newDirection) {
            case "G":
                return Direction.N;
            case "D":
                return Direction.S;
            default:
                return null;
        }
    }

    @Override
    public Coordinates makeaMove(Coordinates oldCoordinates) {
        return Coordinates.builder().x(oldCoordinates.getX() + 1).y(oldCoordinates.getY()).build();
    }
}
