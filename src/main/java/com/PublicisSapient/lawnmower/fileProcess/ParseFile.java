package com.PublicisSapient.lawnmower.fileProcess;

import com.PublicisSapient.lawnmower.entities.Lawn;
import com.PublicisSapient.lawnmower.entities.Tasks;
import com.PublicisSapient.lawnmower.exceptions.FileContentNotCorrectException;

public class ParseFile {


    /**
     * This Methodes parse everyLine and set the result in the Lawn object
     *
     * @param line
     * @param lineIndex
     * @param lawn
     * @return
     */
    public static Lawn parseline(String line, int lineIndex, Lawn lawn) {
        if (LineMatcher.isPositionValid(line)) {
            lawn.setStart(ObjectBuilder.getPosition(line));
        } else if (LineMatcher.isInstructionsValid(line)) {
            lawn.setInstructions(ObjectBuilder.getInstructions(line));
        } else if (!LineMatcher.isDimentionsValid(line)) {
            throw new FileContentNotCorrectException("Unexpected input in line : " + lineIndex + " with instruction : " + line);
        }
        return lawn;
    }


    /**
     * This methode checks and set the dimentions of the surface in the Object FileTask
     *
     * @param line
     * @param lineIndex
     * @param tasks
     * @return
     */
    public static Tasks parseDimentions(String line, int lineIndex, Tasks tasks) {
        if (lineIndex == 1) {
            if (LineMatcher.isDimentionsValid(line))
                tasks.setDimentions(ObjectBuilder.getCoordonates(line));
            else
                throw new FileContentNotCorrectException("Unexpected input in line : " + lineIndex + " with instruction : " + line);
        }
        return tasks;
    }
}
