package com.PublicisSapient.lawnmower.fileProcess;

public class LineMatcher {

    private static final String DIMENTIONS_PATTERN = "(\\d)(\\s)(\\d)";
    private static final String POSITION_PATTERN = "\\d\\s\\d\\s[NSEW]";
    private static final String INSTRUCTIONS_PATTERN = "^[GDA]*$";


    /**
     * This methode checks if the dimeentions specified in the file are correct
     *
     * @param dimentions
     * @return
     */
    public static boolean isDimentionsValid(String dimentions) {
        return dimentions.matches(DIMENTIONS_PATTERN);
    }


    /**
     * This methide cheks if the positions in the file are correct
     *
     * @param position
     * @return
     */
    public static boolean isPositionValid(String position) {
        return position.matches(POSITION_PATTERN);
    }


    /**
     * This methode checks if the instructions in the file are correct
     *
     * @param instructions
     * @return
     */
    public static boolean isInstructionsValid(String instructions) {
        return instructions.matches(INSTRUCTIONS_PATTERN);
    }
}
