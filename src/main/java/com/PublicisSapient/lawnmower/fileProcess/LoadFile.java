package com.PublicisSapient.lawnmower.fileProcess;

import com.PublicisSapient.lawnmower.entities.Lawn;
import com.PublicisSapient.lawnmower.entities.Tasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class LoadFile {

    private static final Logger logger = LoggerFactory.getLogger(LoadFile.class);

    /**
     * This methode is reading the file from path and setting the instructions into an Tasks object
     *
     * @param path
     * @return an object FileTask with all instructions to start the Lawnmowers
     */
    public static Tasks loadFile(String path) {
        ClassLoader classLoader = LoadFile.class.getClassLoader();
        Tasks fileTask = new Tasks();
        List<Lawn> taskList = new ArrayList<>();
        Lawn lawn = new Lawn();
        try (InputStream inputStream = classLoader.getResourceAsStream(path);
             InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {
            String line;
            int lineIndex = 1;
            while ((line = reader.readLine()) != null) {
                fileTask = ParseFile.parseDimentions(line, lineIndex, fileTask);
                lawn = ParseFile.parseline(line, lineIndex, lawn);
                if (LineMatcher.isInstructionsValid(line)) {
                    taskList.add(lawn);
                    lawn = new Lawn();
                }
                lineIndex++;
            }

        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
        fileTask.setTasks(taskList);
        return fileTask;
    }

}
