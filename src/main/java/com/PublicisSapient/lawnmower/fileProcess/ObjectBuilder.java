package com.PublicisSapient.lawnmower.fileProcess;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;
import com.PublicisSapient.lawnmower.entities.Position;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ObjectBuilder {

    private static final String DIMENTIONS_PATTERN = "(\\d)(\\s)(\\d)";
    private static final String POSITION_PATTERN = "(\\d)(\\s)(\\d)(\\s)([NSEW])";


    /**
     * This methodes transform the line of coordinates to an Object
     *
     * @param dimentions
     * @return
     */
    public static Coordinates getCoordonates(String dimentions) {
        Pattern pattern = Pattern.compile(DIMENTIONS_PATTERN);
        Matcher matcher = pattern.matcher(dimentions);
        while (matcher.find()) {
            return Coordinates.builder().x(Integer.valueOf(matcher.group(1))).y(Integer.valueOf(matcher.group(3))).build();
        }
        return null;
    }

    /**
     * This methode transforme the position line to a position object
     *
     * @param position
     * @return
     */
    public static Position getPosition(String position) {
        Pattern pattern = Pattern.compile(POSITION_PATTERN);
        Matcher matcher = pattern.matcher(position);
        while (matcher.find()) {
            Coordinates coordinates = Coordinates.builder().x(Integer.valueOf(matcher.group(1))).y(Integer.valueOf(matcher.group(3))).build();
            return Position.builder().coordinates(coordinates).direction(Direction.valueOf(matcher.group(5))).build();
        }
        return null;
    }

    /**
     * This Methode transforms the instructions line to a list of Characters
     *
     * @param position
     * @return
     */
    public static List<Character> getInstructions(String position) {
        return position.chars().mapToObj(i -> (char) i).collect(Collectors.toList());
    }
}
