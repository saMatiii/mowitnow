package com.PublicisSapient.lawnmower.exceptions;

public class FileContentNotCorrectException extends RuntimeException {

    public FileContentNotCorrectException(String message) {
        super(message);
    }

}
