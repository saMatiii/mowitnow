package com.PublicisSapient.lawnmower.services;


import com.PublicisSapient.lawnmower.core.DirectionCheck;
import com.PublicisSapient.lawnmower.core.DirectionFactory;
import com.PublicisSapient.lawnmower.entities.*;
import com.PublicisSapient.lawnmower.fileProcess.LoadFile;

public class Launch {

    private static DirectionCheck direction;

    public static Tasks launchLawns(String path) {
        Tasks tasks = LoadFile.loadFile(path);

        for (int i = 0; i < tasks.getTasks().size(); i++) {
            Lawn lawn = tasks.getTasks().get(i);
            lawn.getInstructions().forEach(inst -> {
                if (isRotation(inst.toString())) {
                    direction = DirectionFactory.getDirectionObject(lawn.getStart().getDirection());
                    Direction newRedirection = direction.redirect(inst.toString());
                    lawn.getStart().setDirection(newRedirection);
                } else {
                    direction = DirectionFactory.getDirectionObject(lawn.getStart().getDirection());
                    if (!direction.isNextPositionOut(lawn.getStart().getCoordinates(), tasks.getDimentions())) {
                        Coordinates newCords = direction.makeaMove(lawn.getStart().getCoordinates());
                        lawn.getStart().setCoordinates(newCords);
                    }
                }
            });
            tasks.getTasks().set(i, lawn);
        }
        return tasks;
    }


    public static boolean isRotation(String instruction) {
        for (Rotation c : Rotation.values()) {
            if (c.name().equals(instruction)) {
                return true;
            }
        }
        return false;
    }

}
