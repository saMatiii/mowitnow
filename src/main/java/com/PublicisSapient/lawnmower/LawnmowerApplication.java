package com.PublicisSapient.lawnmower;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LawnmowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LawnmowerApplication.class, args);
	}

}
