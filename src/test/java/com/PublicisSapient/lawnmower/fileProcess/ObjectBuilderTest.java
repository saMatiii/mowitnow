package com.PublicisSapient.lawnmower.fileProcess;

import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;
import com.PublicisSapient.lawnmower.entities.Position;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ObjectBuilderTest {

    private static final Logger logger = LoggerFactory.getLogger(ObjectBuilderTest.class);

    @Test
    public void should_retourn_coordinates_Object_from_input() {
        String input_Coordinates = "1 5";
        Coordinates coordinates = ObjectBuilder.getCoordonates(input_Coordinates);
        Assertions.assertEquals(coordinates.getX(), 1);
        Assertions.assertEquals(coordinates.getY(), 5);
    }

    @Test
    public void should_retourn_Position_Object_from_input() {
        String input_Position = "1 5 N";
        Position position = ObjectBuilder.getPosition(input_Position);
        Assertions.assertEquals(position.getCoordinates().getX(), 1);
        Assertions.assertEquals(position.getCoordinates().getY(), 5);
        Assertions.assertEquals(position.getDirection(), Direction.N);
    }

    @Test
    public void should_retourn_Instractions_array_from_input() {
        String input_Position = "GAGAGAGADA";
        List<Character> instructions = ObjectBuilder.getInstructions(input_Position);
        logger.info(String.valueOf(instructions));
        Assertions.assertEquals(instructions.size(), 10);
        Assertions.assertEquals(instructions.get(0), 'G');
    }

}
