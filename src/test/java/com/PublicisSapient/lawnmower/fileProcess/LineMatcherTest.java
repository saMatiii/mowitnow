package com.PublicisSapient.lawnmower.fileProcess;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class LineMatcherTest {

    @Test
    public void should_return_OK_when_Matching_Dimentions() {
        String input_Dementions_OK = "5 5";
        Assertions.assertTrue(LineMatcher.isDimentionsValid(input_Dementions_OK));
    }

    @Test
    public void should_return_KO_when_Matching_Dimentions() {
        String input_Dementions_KO_1 = "5 5 ";
        String input_Dementions_KO_2 = "5 5 AZE";
        String input_Dementions_KO_3 = "55";
        String input_Dementions_KO_4 = "";
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Dementions_KO_1));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Dementions_KO_2));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Dementions_KO_3));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Dementions_KO_4));
    }

    @Test
    public void should_return_OK_when_Matching_Positions() {
        String input_Position_OK = "1 2 N";
        Assertions.assertTrue(LineMatcher.isPositionValid(input_Position_OK));
    }

    @Test
    public void should_return_KO_when_Matching_Positions() {
        String input_Position_KO_1 = "12 N";
        String input_Position_KO_2 = "1 2 KO";
        String input_Position_KO_3 = "1 2 KOKO";
        String input_Position_KO_4 = "1 2N";
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Position_KO_1));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Position_KO_2));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Position_KO_3));
        Assertions.assertFalse(LineMatcher.isDimentionsValid(input_Position_KO_4));
    }

    @Test
    public void should_return_OK_when_Matching_Instructions() {
        String input_Instruction_OK = "DAADADADAA";
        Assertions.assertTrue(LineMatcher.isInstructionsValid(input_Instruction_OK));
    }

    @Test
    public void should_return_KO_when_Matching_Instructions() {
        String input_Instruction_KO_1 = " AAGA ";
        String input_Instruction_KO_2 = "AGAZZZ";
        String input_Instruction_KO_3 = "AGG123";
        Assertions.assertFalse(LineMatcher.isInstructionsValid(input_Instruction_KO_1));
        Assertions.assertFalse(LineMatcher.isInstructionsValid(input_Instruction_KO_2));
        Assertions.assertFalse(LineMatcher.isInstructionsValid(input_Instruction_KO_3));
    }


}
