package com.PublicisSapient.lawnmower.fileProcess;

import com.PublicisSapient.lawnmower.entities.Direction;
import com.PublicisSapient.lawnmower.entities.Lawn;
import com.PublicisSapient.lawnmower.entities.Tasks;
import com.PublicisSapient.lawnmower.exceptions.FileContentNotCorrectException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileProcessingTest {

    private static final String path = "input/input.txt";
    private static final Logger logger = LoggerFactory.getLogger(FileProcessingTest.class);

    @Test
    public void should_Load_file_OK() {
        Tasks fileTask = LoadFile.loadFile(path);
        logger.info(fileTask.getDimentions().toString());
        List<Character> expected_instruction_1 = Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A');
        List<Character> expected_instruction_2 = Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A');

        Assertions.assertEquals(fileTask.getDimentions().getX(), 5);
        Assertions.assertEquals(fileTask.getDimentions().getY(), 5);

        Assertions.assertEquals(fileTask.getTasks().get(0).getStart().getCoordinates().getX(), 1);
        Assertions.assertEquals(fileTask.getTasks().get(0).getStart().getCoordinates().getY(), 2);
        Assertions.assertEquals(fileTask.getTasks().get(0).getStart().getDirection(), Direction.N);
        Assertions.assertEquals(fileTask.getTasks().get(0).getInstructions(), expected_instruction_1);

        Assertions.assertEquals(fileTask.getTasks().get(1).getStart().getCoordinates().getX(), 3);
        Assertions.assertEquals(fileTask.getTasks().get(1).getStart().getCoordinates().getY(), 3);
        Assertions.assertEquals(fileTask.getTasks().get(1).getStart().getDirection(), Direction.E);
        Assertions.assertEquals(fileTask.getTasks().get(1).getInstructions(), expected_instruction_2);
    }

    @Test
    public void should_set_Dimentions_from_input() {
        String input_Dimentions = "3 5";
        Tasks tasks = new Tasks();
        Assertions.assertEquals(ParseFile.parseDimentions(input_Dimentions, 1, tasks).getDimentions().getX(), 3);
        Assertions.assertEquals(ParseFile.parseDimentions(input_Dimentions, 1, tasks).getDimentions().getY(), 5);
    }

    @Test
    public void should_throw_exception_when_dimentions_not_valid() {
        String input_Dimentions_KO = "3 5A";
        Tasks tasks = new Tasks();

        Exception exception = Assertions.assertThrows(FileContentNotCorrectException.class, () -> {
            ParseFile.parseDimentions(input_Dimentions_KO, 1, tasks);
        });
        String expectedMessage = "Unexpected input in line :";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void should_set_Position_from_input() {
        String input_Dimentions = "1 3 N";
        Lawn lawn = new Lawn();
        Assertions.assertEquals(ParseFile.parseline(input_Dimentions, 2, lawn).getStart().getCoordinates().getX(), 1);
        Assertions.assertEquals(ParseFile.parseline(input_Dimentions, 2, lawn).getStart().getCoordinates().getY(), 3);
        Assertions.assertEquals(ParseFile.parseline(input_Dimentions, 2, lawn).getStart().getDirection(), Direction.N);
    }

    @Test
    public void should_throw_exception_when_Position_not_valid() {
        String input_Dimentions_KO = "1 3 NN";
        Lawn lawn = new Lawn();

        Exception exception = Assertions.assertThrows(FileContentNotCorrectException.class, () -> {
            ParseFile.parseline(input_Dimentions_KO, 2, lawn);
        });
        String expectedMessage = "Unexpected input in line :";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void should_set_Instructions_from_input() {
        String input_Instructions = "GAGAGAGAA";
        List<Character> expected_instructions = Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A');

        Lawn lawn = new Lawn();
        Assertions.assertEquals(ParseFile.parseline(input_Instructions, 2, lawn).getInstructions(), expected_instructions);
    }

    @Test
    public void should_throw_exception_when_Instructions_not_valid() {
        String input_Instructions_KO = "KO INSTRUCTIONS";
        Lawn lawn = new Lawn();
        Exception exception = Assertions.assertThrows(FileContentNotCorrectException.class, () -> {
            ParseFile.parseline(input_Instructions_KO, 2, lawn);
        });
        String expectedMessage = "Unexpected input in line :";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

}
