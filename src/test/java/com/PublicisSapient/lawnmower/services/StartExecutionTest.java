package com.PublicisSapient.lawnmower.services;

import com.PublicisSapient.lawnmower.entities.Direction;
import com.PublicisSapient.lawnmower.entities.Tasks;
import com.PublicisSapient.lawnmower.fileProcess.FileProcessingTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class StartExecutionTest {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessingTest.class);

    @Test
    public void should_return_true_when_rotation_param() {
        String rotation_left = "G";
        String rotation_tight = "D";
        Assertions.assertTrue(Launch.isRotation(rotation_left));
        Assertions.assertTrue(Launch.isRotation(rotation_tight));
    }

    @Test
    public void should_return_false_when_Not_rotation_param() {
        String rotation_KO_1 = "A";
        String rotation_KO_2 = "d";
        assertFalse(Launch.isRotation(rotation_KO_1));
        assertFalse(Launch.isRotation(rotation_KO_2));
    }

    @Test
    public void should_return_expected_Last_Positions() {
        String path = "input/input.txt";

        Tasks tasks = Launch.launchLawns(path);

        assertEquals(tasks.getTasks().get(0).getStart().getCoordinates().getX(), 1);
        assertEquals(tasks.getTasks().get(0).getStart().getCoordinates().getY(), 3);
        assertEquals(tasks.getTasks().get(0).getStart().getDirection(), Direction.N);
        assertEquals(tasks.getTasks().get(1).getStart().getCoordinates().getX(), 5);
        assertEquals(tasks.getTasks().get(1).getStart().getCoordinates().getY(), 1);
        assertEquals(tasks.getTasks().get(1).getStart().getDirection(), Direction.E);

        logger.info(tasks.getTasks().get(0).getStart().getCoordinates().toString());
        logger.info(tasks.getTasks().get(0).getStart().getDirection().toString());
        logger.info(tasks.getTasks().get(1).getStart().getCoordinates().toString());
        logger.info(tasks.getTasks().get(1).getStart().getDirection().toString());

    }


}
