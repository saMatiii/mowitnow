package com.PublicisSapient.lawnmower.services;

import com.PublicisSapient.lawnmower.core.DirectionCheck;
import com.PublicisSapient.lawnmower.core.DirectionFactory;
import com.PublicisSapient.lawnmower.entities.Coordinates;
import com.PublicisSapient.lawnmower.entities.Direction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DirectionCheckTest {

    private DirectionCheck directionCheck;

    @Test
    public void should_return_true_when_next_Position_out() {
        Coordinates dimentions = Coordinates.builder().x(5).y(5).build();

        Coordinates actual_Position_N = Coordinates.builder().x(2).y(5).build();
        Coordinates actual_Position_S = Coordinates.builder().x(2).y(0).build();
        Coordinates actual_Position_E = Coordinates.builder().x(5).y(2).build();
        Coordinates actual_Position_W = Coordinates.builder().x(0).y(2).build();

        assertTrue(DirectionFactory.getDirectionObject(Direction.N).isNextPositionOut(actual_Position_N, dimentions));
        assertTrue(DirectionFactory.getDirectionObject(Direction.S).isNextPositionOut(actual_Position_S, dimentions));
        assertTrue(DirectionFactory.getDirectionObject(Direction.E).isNextPositionOut(actual_Position_E, dimentions));
        assertTrue(DirectionFactory.getDirectionObject(Direction.W).isNextPositionOut(actual_Position_W, dimentions));
    }

    @Test
    public void should_passe_when_next_Position_IN() {
        Coordinates dimentions = Coordinates.builder().x(5).y(5).build();

        Coordinates actual_Position = Coordinates.builder().x(2).y(2).build();

        assertFalse(DirectionFactory.getDirectionObject(Direction.N).isNextPositionOut(actual_Position, dimentions));
        assertFalse(DirectionFactory.getDirectionObject(Direction.S).isNextPositionOut(actual_Position, dimentions));
        assertFalse(DirectionFactory.getDirectionObject(Direction.E).isNextPositionOut(actual_Position, dimentions));
        assertFalse(DirectionFactory.getDirectionObject(Direction.W).isNextPositionOut(actual_Position, dimentions));
    }

    @Test
    public void should_return_Correct_redirection_from_left() {
        String newDirection = "G";
        assertEquals(DirectionFactory.getDirectionObject(Direction.N).redirect(newDirection), Direction.W);
        assertEquals(DirectionFactory.getDirectionObject(Direction.W).redirect(newDirection), Direction.S);
        assertEquals(DirectionFactory.getDirectionObject(Direction.S).redirect(newDirection), Direction.E);
        assertEquals(DirectionFactory.getDirectionObject(Direction.E).redirect(newDirection), Direction.N);
    }

    @Test
    public void should_return_Correct_redirection_from_Right() {
        String newDirection = "D";
        assertEquals(DirectionFactory.getDirectionObject(Direction.N).redirect(newDirection), Direction.E);
        assertEquals(DirectionFactory.getDirectionObject(Direction.W).redirect(newDirection), Direction.N);
        assertEquals(DirectionFactory.getDirectionObject(Direction.S).redirect(newDirection), Direction.W);
        assertEquals(DirectionFactory.getDirectionObject(Direction.E).redirect(newDirection), Direction.S);
    }

    @Test
    public void sould_move_from_position_to_another() {
        Coordinates actuel_position = Coordinates.builder().x(1).y(2).build();
        assertEquals(DirectionFactory.getDirectionObject(Direction.E).makeaMove(actuel_position).getX(), 2);
        assertEquals(DirectionFactory.getDirectionObject(Direction.W).makeaMove(actuel_position).getX(), 0);
        assertEquals(DirectionFactory.getDirectionObject(Direction.N).makeaMove(actuel_position).getY(), 3);
        assertEquals(DirectionFactory.getDirectionObject(Direction.S).makeaMove(actuel_position).getY(), 1);
    }

}
